import UIKit
import MapKit
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configMapView()
    }
    
    private func configMapView() {
        
        let latitude = 44.433769;
        let longitude = 26.0544842;
        
        let center = CLLocationCoordinate2D(latitude: latitude,
                                            longitude: longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03))
        
        self.mapView.setRegion(region, animated: true)
        
        self.mapView.showsUserLocation = true;
    }
    
    @IBAction func call(_ sender: UIButton) {
        
        let schema = "tel://"
        let telephone = "0040727761355"
        let string = schema + telephone
        // tel://0040727761355
        
        let url = URL(string: string)
        
        if UIApplication.shared.canOpenURL(url!) {
            
            UIApplication.shared.open(url!,
                                      options: [:],
                                      completionHandler: nil)
        }
    }
    
    @IBAction func sendEmailButtonPressed(_ sender: UIButton) {
        print ("Pressing!")
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        if let filePath = Bundle.main.path(forResource: "swift", ofType: "png") {
            print("File path loaded.")
            
            if let fileData = NSData(contentsOfFile: filePath) {
                print("File data loaded.")
                mailComposerVC.addAttachmentData(fileData as Data, mimeType: "image/png", fileName: "swift")
            }
        }
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
